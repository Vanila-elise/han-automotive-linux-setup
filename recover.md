How to recover from unpleasent choices


If you manage to kill your system by uninstalling important packages
e.g. apt purge python3 (Don't try it'll kill your system!)

you can help yourself by looking at the log of APT 
/var/log/apt/history.log

To get there, start ubuntu in rescue mode, 
select start normally
and if you end up with a black screen, you can summon a virtual terminal with alt + F4 (look out for FnLock)

There you can log in to your user, naviagate to the logfile and find the entry with the mistakes you made.

Then you copy that file to a usb stick
How to mount a usb stick https://vitux.com/how-to-manually-mount-unmount-a-usb-device-on-ubuntu/

With that usb stick, you can edit that file on another computer (there's also possiblities in your broken ubutu, but it's easier on another computer) with Notepad++.

Search and replace with following regular expression:
(?=:).+?(?=,)
replace with an empty space (it does not work if you enter nothing to replace with)
this will remove all the stuff between the : and the comma e.g. :amd64 (11.1.0ubuntu4), while leaving the comma there.
This gives you a list of packages to reinstall, separated by commas.

then remove the commas and replace them with a newline (\n)

Do some last cosmetic edits (The last entry won't be formatted propoerly, so do that manually )

Then save the file, put it in your broken computer, remount, go to the folder with the file, and call:
' xargs sudo apt-get -y install <file.txt ' (> only for markdown)

Hopefully that will do the trick for you!
