


#Docker

## Installation

*Advice:*
It is adviced to install via 'Install using the repository'

Follow the [manual](https://docs.docker.com/engine/install/ubuntu/)

*Advice:*
Do not forget to follow [Post-installation steps for Linux](https://docs.docker.com/engine/install/linux-postinstall/)

*Advice:*
If you have trouble with network-connectings inside the docker enviroment follow:
Configure where the Docker daemon listens for connections
in post installation steps for linux

*Advice:* 
If you receive the error message
```bash
docker: Error response from daemon: Unknown runtime specified nvidia
```
you need to install CUDA drivers and get the nvidia docker deamon.
- [NVIDIA CUDA Installation Guide for Linux](https://docs.nvidia.com/cuda/cuda-installation-guide-linux/index.html)
- [nvidia-docker2](https://docs.nvidia.com/datacenter/cloud-native/container-toolkit/install-guide.html)
