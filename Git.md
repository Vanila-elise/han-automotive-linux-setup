## Gitlab
The wiki is hosted on [Gitlab](https://gitlab.com/aibot/han-automotive-linux-setup).
It is advised to copy the wiki [via SSH](https://docs.gitlab.com/ee/ssh/README.html#options-for-microsoft-windows). Then the username and password don't need to be given each time.

## Git
Git can be installed via the desired package manager on most linux systems.
On Windows Systems it is advised to use a desktop client like gitcraken.
For now commiting and pushing to the master is allowed!
