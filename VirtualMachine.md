> This is a document meant to address the issues that might occur
> when trying to run autoware/ROS and docker using an Ubuntu 
> that is installed  Virtual Machine (In this case - VMWare Player)


## Set up
It is possible to use VMWare Player to run the required Ubuntu 18.04 LTS.
Using a Virtual Machine is a comfortable way to combine work in Microsoft Windows with the work on linux.
The Set up is very simple and quite intuative.

An example of a tutorial to set up a VM:
(https://linuxhint.com/install_ubuntu_vmware_workstation/)

After this installation you will have access to the linux distribution that you've installed.

## Usage Problems
Because the Hardware that the VM-OS is simulated, and in theory on one pc many VM could run, the 
Virtual machine Hypervisor (what simulates the hardware so to say) does not allow direct access to the hardware.
This means that - in our case- when trying to run autoware with (NVIDIA) CUDA - no NVIDIA device can be found even
if the real hardware is INVIDIA based.

## Solution

There are 2 solutions: and easy one and a difficult one:
* Difficult: [these solutions were NOT tested]
 * gpuocelot => (https://github.com/gtcasl/gpuocelot) {Ocelot builds as a shared library that replaces NVIDIA's CUDA Runtime API (libcudart)}
 * GVirtus => (https://github.com/cjg/GVirtuS)  {2 machines, one with CUDA as a server, the other without as a client} 
 * rCUDA => (http://rcuda.net/index.php/what-s-rcuda.html) {remote CUDA - might work but requires you te REQUEST the software which is a drawback}

* Easy:
 * Compile or Run Autoware.AI without CUDA:
      In the documentation it is said that CUDA is not required, but in reality it is set ON by default.


## Easy Solution:
There are two script files that are used: ./run.sh and ./build.sh
Both contain the default settings, and will NOT work using VM and the default settings.
```# Default settings
CUDA="on"
IMAGE_NAME="autoware/autoware"
TAG_PREFIX="latest"
ROS_DISTRO="melodic"
BASE_ONLY="false"
PRE_RELEASE="off"
AUTOWARE_HOST_DIR=""
USER_ID="$(id -u)"
```

We need to change it to:
```# Default settings
CUDA="off"
IMAGE_NAME="autoware/autoware"
TAG_PREFIX="latest"
ROS_DISTRO="melodic"
BASE_ONLY="false"
PRE_RELEASE="off"
AUTOWARE_HOST_DIR=""
USER_ID="$(id -u)">
```

Then another issue that (currently) arises is that the USER_ID is incorrect. the workaround for it right now is to change the USER_ID field to a random number.
