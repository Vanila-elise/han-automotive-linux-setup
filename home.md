** Table of contents**

[[_TOC_]]


# General




## How to write a wiki

A wiki is a living, breathing encyclopedia where anyone involved can add their own pages, articles and knowledge. Contrary to wikipedia, this wiki is for the specific purpose of the computer and program setup within the HAN automotive.

# In case you messed up

[How to recover](./recover.md)

# Ubuntu Setup

## Installation medium
To install any linux distribution, the easiest way is to use a USB-stick. Finding one of sufficent size should not be a problem (8GB+).

## Unetbootin
[UNetbootin](https://unetbootin.github.io/) allows you to create bootable Live USB drives for Ubuntu and other Linux distributions without burning a CD. A nice feature of this tool is that it can download the needed ISO by itself.

## Ubuntu 20.04 (with Autoware in Docker)
It is now adviced to run Autoware within a docker environemnt to improve protability and compatibility. This is why the latest LTS version of Ubuntu should be used because Docker is Host-agnostic and the latest version should provide best compatibility with all systems (unless you have a special case but you'll know  ;-) ).

### Pre-installation procedures

**Caution!**
Before installing (due to experiences with Ubuntu 16.04) it is strongly advised to physically remove any other drives containing other operating systems like Windows from the machine. Due to a bug, even when carefully choosing the installation settings (in Ubuntu 16.04, maybe also later), operating systems on other drives can suffer damage or be destroyed during installation.

**Caution!** 
when choosing "erase disk and install Ubuntu 20.04" the disk will be wiped!

### Installation manual

**Manual**
To install Ubuntu, remove all other HDDs and SSDs from your computer except the one you want to install on, and insert the stick prepared with unetbootin. Via the boot priority select the USB drive to boot from. Follow the step by step guide. Select "erase disk and install Ubuntu 20.04" (you can select other options if you know what you're doing).
At one point, you will see an option to install additional thrid party drivers. Select that option. If you're using secure boot, make sure to set a password to turn off secure boot for Ubuntu. After the installation process finishes, you will be asked to remove the installation media and reboot.  

### Post installation procedures

*Advice:* 
If Ubuntu gets stuck in the reboot process shutdown the computer by pressing the on/off button long.

**Caution!**  
If after reboot, you will see a blue screen with MOK (Machine-Owner Key) management you will need to press any key to stop it from booting normally.
Inside the MOK choose to [change secure boot state](https://wiki.ubuntu.com/UEFI/SecureBoot/DKMS). When asked for a key, provide the password you chose during the installation of Ubuntu. 
It is important to follow that procedure. If you miss the MOK management screen when using secure boot, you will need to reinstall ubuntu (except if you know how to fix it otherwise).

### Further steps

[Install Docker](./docker.md)

[Install Autoware](./autoware.md)





## Ubuntu 16.04
To run Proj ect Aslan, an installation of [Ubuntu 16.04 is needed](https://github.com/project-aslan/Aslan#--ubuntu-1604-lts).

**Caution!**
Before installing Ubuntu 16.04 it is strongly advised to physically remove any other drives containing other operating systems like Windows from the machine. Due to a bug, even when carefully choosing the installation settings, operating systems on other drives can suffer damage or be destroyed during installation.

*Advice:*
To use Hybernation mode, the size of the swap partition needs to be at least the size of the RAM.
Either choose your own partition scheme during installation or boot again form the USB-drive and[change the size of the swap partition with Gparted](https://howtoubuntu.org/how-to-resize-partitions-with-the-ubuntu-or-gparted-live-cd).
If you have to use secure boot (e.g. you're using a HAN-workstation) you might want to have a look [here](https://askubuntu.com/questions/1106105/18-04-hibernate-with-uefi-and-secure-boot-enabled) to enable hibernation.

*Advice:*
In case you want to stay on your current Ubuntu version, go to software&Updates --> Updates and turn off notifications for newer ubuntu versions

**Manual**
To install Ubuntu, remove all other HDDs and SSDs from your computer except the one you want to install on, and insert the stick prepared with unetbootin. Via the boot priority select the USB drive to boot from. Follow the step by step guide. Select "erase disk and install Ubuntu 16.04" (you can select other options if you know what you're doing).
At one point, you will see an option to install additional thrid party drivers. Select that option. If you're using secure boot, make sure to set a password to turn off secure boot for Ubuntu. After the installation process finishes, you will be asked to remove the installation media and reboot. Often Ubuntu gets stuck in the reboot process. If that happens, shutdown the computer by pressing the on/off button long. 
After reboot, you will see a blue screen with MOK (Machine-Owner Key) management. Probably you will need to press any key to stop it from booting normally.
Inside the MOK choose to [change secure boot state](https://wiki.ubuntu.com/UEFI/SecureBoot/DKMS). When asked for a key, provide the password you chose during the installation of Ubuntu. 
It is important to follow that procedure. If you miss the MOK management screen when using secure boot, you will need to reinstall ubuntu (except if you know how to fix it otherwise).

*Advice:*
Make sure, you enroll the MOK properly with each driver update, otherwise the OS could become unusable.


## Graphics drivers
In a next step after the installation of Ubuntu, the newest graphics drivers for your nvidia card need to be installed. As standard, Ubuntu uses the nouveau driver (open source). For our purposes here, we need the  NVIDIA binary drivers, ideally in the latest LTS version.
These drivers can be [loaded as a PPA](https://launchpad.net/~graphics-drivers/+archive/ubuntu/ppa). The current long-lived branch release is 'nvidia-430'. 
To add the PPA to your system press <kbd>ctrl</kbd> + <kbd>alt</kbd> + <kbd>t</kbd> to open a terminal. Then execute following lines:
```bash
sudo add-apt-repository ppa:graphics-drivers/ppa
sudo apt-get update
```
The actual graphics driver is then installed with following lines:
```bash
sudo apt install nvidia-430
```
You can check the installed driver then under Software&Updates &rarr; Additional Drivers or with
```bash
nvidia-smi
```




# Software to install

## Project Aslan

[Project Aslan](https://www.project-aslan.org/) is an Open-Source Self-Driving Software for Low-Speed Environments.

For setting up, follow the guide on [Github](https://github.com/project-aslan/Aslan)


## Autoware
[Autoware]() is an "all-in-one" open-source software for self-driving vehicles.

For setting up, follow the [guide](https://autoware.readthedocs.io/en/feature-documentation_rtd/UsersGuide/Installation.html) in the [autoware documentation](https://autoware.readthedocs.io/en/feature-documentation_rtd/index.html)

## Docker
It is highly advised to set up Autoware within a docker environment. This provides better portability for collaboration with other students and future projects.
To install the docker engine follow the [instructions on Docker docs](https://docs.docker.com/engine/install/ubuntu/).
