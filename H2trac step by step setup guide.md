Set up linux in dual boot with windows
If you need space, go to the partitioning tool and resize the windows partition to make unallocated space 
ideally use Ubuntu 20.04, (if not possible e.g. due to secure boot, use 22.04)
get here:
https://old-releases.ubuntu.com/releases/
flash it on a usb stick with Rufus
https://rufus.ie/en/
good settings are: GPT partitioning for EFI and DD mode 
(renders the stick unusable for other tings until reformatting, but highest chance of success for this step)
run the installation
select to install additional drivers (have wifi or even better Ethernet connection available)

switch to latest graphics drivers

set up SSH keys
https://docs.gitlab.com/ee/user/ssh.html
ssh-keygen -t rsa -b 2048
enter, enter, enter

add ssh keys to your gitlab account
https://docs.gitlab.com/ee/user/ssh.html#add-an-ssh-key-to-your-gitlab-account
xclip -sel clip < ~/.ssh/id_rsa.pub
Sign in to GitLab.
On the top bar, in the top right corner, select your avatar.
Select Preferences.
On the left sidebar, select SSH Keys.
In the Key box, paste the contents of your public key. If you manually copied the key, make sure you copy the entire key, which starts with ssh-rsa, ssh-dss, ecdsa-sha2-nistp256, ecdsa-sha2-nistp384, ecdsa-sha2-nistp521, ssh-ed25519, sk-ecdsa-sha2-nistp256@openssh.com, or sk-ssh-ed25519@openssh.com, and may end with a comment.
In the Title box, type a description, like Work Laptop or Home Workstation.
Optional. Update Expiration date to modify the default expiration date. Ideally set to never expire.
Select Add key

docker installation 
https://docs.docker.com/engine/install/ubuntu/

cuda installation  (Nvidia container toolkit)
https://docs.nvidia.com/datacenter/cloud-native/container-toolkit/install-guide.html
Setup the package repository and the GPG key:

distribution=$(. /etc/os-release;echo $ID$VERSION_ID) \
      && curl -fsSL https://nvidia.github.io/libnvidia-container/gpgkey | sudo gpg --dearmor -o /usr/share/keyrings/nvidia-container-toolkit-keyring.gpg \
      && curl -s -L https://nvidia.github.io/libnvidia-container/$distribution/libnvidia-container.list | \
            sed 's#deb https://#deb [signed-by=/usr/share/keyrings/nvidia-container-toolkit-keyring.gpg] https://#g' | \
            sudo tee /etc/apt/sources.list.d/nvidia-container-toolkit.list

Install the nvidia-docker2 package (and dependencies) after updating the package listing:

sudo apt-get update

sudo apt-get install -y nvidia-docker2

Restart the Docker daemon to complete the installation after setting the default runtime:

sudo systemctl restart docker

At this point, a working setup can be tested by running a base CUDA container:

sudo docker run --rm --gpus all nvidia/cuda:11.0.3-base-ubuntu20.04 nvidia-smi

Install ADE:
https://ade-cli.readthedocs.io/en/latest/install.html
Linux x86_64 and aarch64

    Verify that the Requirements above are fulfilled

    To make ade available globally, install it somewhere in your PATH: * echo $PATH will print a list of possible paths * Commonly used paths are ~/.local/bin and /usr/local/bin

    Download the statically-linked binary from the Releases page of the ade-cli project: e.g. for x86_64

$ cd /path/from/step/above
$ wget https://gitlab.com/ApexAI/ade-cli/-/jobs/1341322851/artifacts/raw/dist/ade+x86_64
$ mv ade+x86_64 ade
$ chmod +x ade
$ ./ade --version
4.3.0
$ ./ade update-cli
$ ./ade --version
<latest-version>


ADE needs a directory on the host machine which is mounted as the user's home directory within the container. The directory is populated with dotfiles, and must be different than the user's home directory outside of the container. In the event ADE is used for multiple, projects it is recommended to use dedicated adehome directories for each project.
ADE looks for a directory containing a file named .adehome starting with the current working directory and continuing with the parent directories to identify the ADE home directory to be mounted.

$ mkdir -p ~/adehome
$ cd ~/adehome
$ touch .adehome

For ADE to function, it must be properly configured. Autoware.Auto provides an .aderc file which is expected to exist in the current working directory, or in any parent directory. Additionally, default configuration values can be overridden by setting environment variables. See the ade --help output for more information about using environment variables to define the configuration.

$ cd ~/adehome
$ git clone <repo>
repo= 
https://gitlab.com/durablecase/simulation/han/gazebo_simulation
cd gazebo_simulation/ade_image/

docker build -t <image_name>:<tag> .   (don't forget the dot!)
image_name = autoware_master_image
tag = v3
(takes time)

Go to 
https://gitlab.com/durablecase/student-groups/automotionstudents-sem6_summer2022/h2trac-chaserbin-coorporative-tools
click on autoware auto to get to
https://gitlab.com/HassanHotait/AutowareAuto
cd ~/adehome/gazebo_simulation
git clone https://gitlab.com/HassanHotait/AutowareAuto

get the usb stick with files from Karl / Yanzheng
plug it in 
The usb stick should standard mount to  /media/$USER 
so go to 
cd /media/$USER 
ls (to get the name of your usb-stick)
cd <name of your usb-stick>
if it does not mount automatically: https://vitux.com/how-to-manually-mount-unmount-a-usb-device-on-ubuntu/
cd Autoware
you'll find a folder src
overwrite the src folder in your existing files with this one with:
cp -rf src ~/adehome/gazebo_simulation/AutowareAuto


cd ~/adehome/gazebo_simulation/AutowareAuto
sudo nano .aderc
edit version of autoware_master_image from :v2 to :v3
add nvidia Cuda
docker image list 
you'll find something like 
nvidia/cuda:11.0.3-base-ubuntu20.04
add it to the ADE_IMAGES

make sure you are in the folder holding the .aderc-file

ade start
ade enter

cd ~/gazebo_simulation/AutowareAuto

Building Autoware Auto
echo "export export RMW_IMPLEMENTATION=rmw_cyclonedds_cpp" >> ~/.bashrc
git checkout master
vcs import < autoware.auto.$ROS_DISTRO.repos
colcon build --cmake-args -DCMAKE_BUILD_TYPE=Release 

cd ../simulation_main 
colcon build

cd adehome
mkdir Durablecase_ws
mkdir src
git clone https://gitlab.com/durablecase/toolbox/ros2-interfaces
git clone https://gitlab.com/durablecase/toolbox/ros2-behavior-trees

source ~/gazebo_simulation/AutowareAuto/install/setup.bash
colcon build --symlink-install

outside of ADE
cd ~adehome
mkdir .local
cd .local
mkdir bin 

go to usb stick 
copy contents of bin folder to your bin folder (outside of ADE)
cd /media/$USER/<name of your usb stick>
cp -rf bin ~/adehome/.local 

back into your ADE
cd 
cd .local/bin
ls -al (you'll see you don't have rights to execute)
sudo chmod +x * (changes rights of all files)
check again with 
ls -al

start the simulation with 
./DurableCaseH2tracSim

new terminal, ade enter, cd .local/bin now every time

./DurableCaseMas voa Robot0
./DurableCaseMas voa Robot1
./DurableCaseMas voa Robot2
./DurableCaseMas mla
./DurableCaseMas ca
./DuiH2trac




