# Setup Guide for Linux computers at HAN Automotive

This wiki documents the setup of a Ubuntu 16.04 system for HAN automotive to work on project ASLAN. Later also guides for 18.04 and 20.04 for Autoware and i-at projects and its successors will follow.

Here will be also a collection of cheat sheets for using linux command line and ROS/Autoware.

The Main page of the wiki is [home](./home.md)
